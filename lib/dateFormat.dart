import 'package:intl/intl.dart';

class DateFormatr {
  static String formatDateTime(DateTime dateTime){
    DateFormat dateFormat = DateFormat.yMMMM().add_jm();
    return dateFormat.format(dateTime);
  }
}
