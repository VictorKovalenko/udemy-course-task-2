import 'package:flutter/material.dart';
import 'package:test9/dateformat.dart';
import '';
import 'package:intl/intl.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'app',
      theme: ThemeData.dark(),
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AppBar'),
      ),
      body: Column(
        children: [
          Container(
            child: Text(DateFormatr.formatDateTime(DateTime.now())),
          ),

        ],
      ),
    );
  }
}
